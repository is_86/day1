#inlcude <stdio.h>

main() {

    float celsius, fahr;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    celsius = lower;
    printf("Celsius to fahrenheit from 0 to 300 with 20 step\n");
    while(celsius <= upper) {
        fahr = celsius * (9-0/5.0) + 32;
        printf("%6.1f %3.0f \n", celsius, fahr);
        celsius = celsius +step;
    }
}
