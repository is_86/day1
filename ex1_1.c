#include <stdio.h>

int main() {

    printf("Hello world\n");
}

/*if you leaave #include <stdio.h> away, it gives warning on missing include but compilees and runs anyway. If you live int from function away, it gives you warning but works, if you put the Hello world to two rows without changin anything it gives you many strange (wrong) errors, but compiles and runs normally. This is used with Linux Mint and GCC.
